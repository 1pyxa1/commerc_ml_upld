# -*- coding: utf-8 -*-
"""
Бизнес-логика скрипта commerc_ml_upld

@author: syshchenko
"""

# TODO добавить запуск процедуры в MySQL для переливки данных из raw таблиц
# TODO добавить typing
# TODO добавить XmlFile class
# TODO переделать ветвление на starter
# TODO добавить unit тесты
# TODO переделать все системные пути на использование Path

import json
import sys
import re
import logging
from os import listdir
from os.path import isfile, join
from collections import namedtuple, OrderedDict

import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.exc import SQLAlchemyError, ProgrammingError
from sqlalchemy.types import LargeBinary, Boolean, DateTime
from sqlalchemy.sql import text as sa_text
from lxml import etree
from pathlib import Path

import utils

sys._enablelegacywindowsfsencoding()

LOG = logging.getLogger()

GUID_PATTERN = re.compile(
        "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}")
DATE_1C_PATTERN = re.compile(
        "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}")
SHORT_OBJ_NAMES = {
        'InformationRegisterRecordSet': 'регсв',
        'CatalogObject': 'спр',
        'DocumentObject': 'док',
        'AccumulationRegisterRecordSet': 'регнак'
        }

CML_TABLES = (
        'InformationRegisterRecordSet',
        'CatalogObject',
        'DocumentObject',
        'AccumulationRegisterRecordSet'
    )


with open('skip_tables.json') as file:
    TMP_DONE_TABLE = json.load(file)


class MysqlConn:
    """
    Класс для подключения к MySQL, в стадии разработки
    """
    def __init__(self, conf_json, sec_json):

        with open(conf_json) as f_conf:
            conf = json.load(f_conf)

        with open(sec_json) as f_sec:
            sec = json.load(f_sec)

        self.engine = None

        self.connection_string = "mysql+mysqlconnector://{}:{}@{}:{}/{}" \
            "?charset=utf8mb4;collation=utf8mb4_unicode_ci;" \
            "use_pure=True".format(
                sec["mysql_user"],
                sec["mysql_pass"],
                conf['mysql_host'],
                conf['mysql_port'],
                conf["mysql_db"])

    def connect(self):
        """
        Создаёт объект engine, который подключается к MySQL
        """
        self.engine = create_engine(self.connection_string)

    def dispose(self):
        """
        Закрывает соединение
        """
        self.engine.dispose()


class XmlFile:
    """
    Класс обёртка файла выгрузки XML из 1С
    """
    def __init__(self, xml_path):
        self.xml_path = xml_path
        self.context = self._get_context

    def __repr__(self):
        return 'XmlFile {}'.format(utils.get_file_name_from_path(
            self.xml_path))

    def _get_context(self):
        return etree.iterparse(self.xml_path, events=('end',))

    @classmethod
    def _get_message_no(cls, context) -> int:
        """
        Парсит до первого вхождения элементы контекста и возвращает номер
        файла, который является уникальным и инкрементным для обработки в базе
        """
        message_no_: int = -1
        for event, elem in context:
            if event == "end" and \
                    elem.tag == "{http://v8.1c.ru/messages}MessageNo":
                message_no_ = int(elem.text)
                break
        return message_no_

    @classmethod
    def _parse_table_and_count_elements(cls, context):
        """
        Возвращает словарь вида Объект - Количество элементов. Элементы
        контекста Регистр Сведений и Накопления содержат вложенные строки,
        поэтому сделан дополнительный цикл.
        """

        groups = {}

        for count, context_tuple in enumerate(context):
            event, elem = context_tuple

            utils.counter(count)

            if event == "end" and any(
                    table_name in elem.tag for table_name in CML_TABLES
            ):
                if any(tag in elem.tag for tag in (
                        'CatalogObject', 'DocumentObject')):
                    try:
                        groups[elem.tag] += 1
                    except KeyError:
                        groups[elem.tag] = 1

                elif any(tag in elem.tag for tag in (
                        'InformationRegisterRecordSet',
                        'AccumulationRegisterRecordSet')):
                    for _ in elem[1]:
                        try:
                            groups[elem.tag] += 1
                        except KeyError:
                            groups[elem.tag] = 1

                elem.clear()

                for ancestor in elem.xpath('ancestor-or-self::*'):
                    while ancestor.getprevious() is not None:
                        del ancestor.getparent()[0]

        return groups

    def describe(self, ext: bool = False):
        """
        Возвращает namedtuple с характеристиками xml файла выгрузки
        """

        if ext:
            descr_nt = namedtuple(
                'DESCRIPTION',
                'db_prefix file_name msg_no tables')
        else:
            descr_nt = namedtuple(
                'DESCRIPTION',
                'db_prefix file_name msg_no')

        db_prefix = utils.get_prefix_from_file(self.xml_path)
        file_name = utils.get_file_name_from_path(self.xml_path)
        msg_no = XmlFile._get_message_no(self.context())

        if ext:
            tables = XmlFile._parse_table_and_count_elements(self.context())
            descr = descr_nt(db_prefix, file_name, msg_no, tables)
        else:
            descr = descr_nt(db_prefix, file_name, msg_no)

        return descr

    def iter_xml(self, filter_elem: tuple = None):
        """
        Функция итерирует контекст, учитывает смену объекта. Отдаёт итеративно
        готовые таблицы на внешнюю обработку
        """
        first_time = True
        rows_list = []
        for count, context_tuple in enumerate(self.context()):
            event, elem = context_tuple
            utils.counter(count)

            if event == "end" and any(
                    table_name in elem.tag for table_name in CML_TABLES):

                filtered = True
                if filter_elem and elem.tag not in filter_elem:
                    filtered = False

                if filtered and elem.tag not in TMP_DONE_TABLE['tables']:

                    # FIXME внести функцию в класс
                    data = process_element(elem)

                    if first_time:
                        previous_elem_tag = elem.tag
                        first_time = False

                    if elem.tag == previous_elem_tag:
                        if isinstance(data, list):
                            rows_list += data
                        else:
                            rows_list.append(data)

                    else:
                        yield rows_list, previous_elem_tag

                        if isinstance(data, list):
                            rows_list = data
                        else:
                            rows_list = [data]

                        previous_elem_tag = elem.tag

                elem.clear()

                for ancestor in elem.xpath('ancestor-or-self::*'):
                    while ancestor.getprevious() is not None:
                        del ancestor.getparent()[0]

        if len(rows_list) > 0:
            yield rows_list, previous_elem_tag


def iter_xml(context, func, engine_, file_name):
    """
    Функция итерирует контекст, учитывает смену объекта. Слишком объёмная,
    требует рефакторинга.
    """
    message_no: int = get_message_no(context)
    table_prefix = utils.get_prefix_from_file(file_name)

    first_time = True
    rows_list = []
    for count, context_tuple in enumerate(context):
        event, elem = context_tuple
        if count % 1000000 == 0:
            sys.stdout.write('\r')
            sys.stdout.write("{}m elements done ".format(count/1000000))
            sys.stdout.flush()

        if event == "end" and any(
                table_name in elem.tag for table_name in CML_TABLES):

            if elem.tag not in TMP_DONE_TABLE['tables']:
                data = func(elem)

                if first_time:
                    previous_elem_tag = elem.tag
                    first_time = False

                if elem.tag == previous_elem_tag:
                    if isinstance(data, list):
                        rows_list += data
                    else:
                        rows_list.append(data)

                else:
                    status, report = push_df(
                        table_prefix, rows_list, previous_elem_tag, engine_,
                        message_no)
                    if status is False:
                        return False, report, message_no

                    if isinstance(data, list):
                        rows_list = data
                    else:
                        rows_list = [data]

                    previous_elem_tag = elem.tag

            elem.clear()
            # Also eliminate now-empty references from the root node to elem
            for ancestor in elem.xpath('ancestor-or-self::*'):
                while ancestor.getprevious() is not None:
                    del ancestor.getparent()[0]

    if len(rows_list) > 0:
        status, err = push_df(
            table_prefix, rows_list, previous_elem_tag, engine_,
            message_no)
        if status is False:
            return False, err, message_no

    del context
    report = 'done'
    return True, report, message_no


def push_df(prefix_name: str,
            rows_list: list,
            previous_elem_tag,
            engine_,
            message_no_
            ) -> tuple:
    """
    Заливка списка (list) строк распарсеных из xml в MySQL через pandas
    dfr.to_sql. Заливка таблицы осуществляется целиком, после смены тэга
    элемента из context, что сказывается на объёме потребляемой оперативной
    памяти при больших регистрах (например Цены). Если в дальнейшем будет
    проблемой, переделать на итеративную заливку. При этом из dfr заливается
    чанками.
    """
    dfr = pd.DataFrame(rows_list)
    dfr['msg_no'] = message_no_

    dfr, dtype = convert_df(dfr, previous_elem_tag)

    table_name = '{}_{}_{}'.format(
        prefix_name,
        SHORT_OBJ_NAMES[previous_elem_tag.split('.')[0]],
        previous_elem_tag.split('.')[1])

    engine_.execute("SET SQL_SAFE_UPDATES=0;")
    del_sql = sa_text(
        '''delete from dev.{} where msg_no >= :msg_no;'''.format(
            table_name)).execution_options(autocommit=True)
    try:
        engine_.execute(del_sql, msg_no=message_no_)
    except ProgrammingError:
        pass

    engine_.execute("SET SQL_SAFE_UPDATES=1;")

    try:
        dfr.to_sql(
            table_name,
            con=engine_,
            if_exists='append',
            chunksize=1000,
            dtype=dtype
            )
        return True, None

    except SQLAlchemyError as err:
        error = str(err.__dict__['orig'])
        return False, error


def convert_df(dfr_, elem_tag_):
    '''
    Переделываем формат столбцов перед заливкой в SQL. GUID конвертируем в
    binary, текстовый true-false в boolean, дата 1С в date. Для определения
    типа столбца используем первые N элементов через df.head(N)
    '''
    dtype = {}

    skip_columns = []
    if elem_tag_ == 'InformationRegisterRecordSet.ЗначенияСвойствОбъектов':
        skip_columns.append('Значение')

    for column in dfr_.head(20):
        if skip_columns:
            if column in skip_columns:
                continue

        for row in dfr_[column]:

            # if not isinstance(row, (str, bytes, bytearray)):
            if not isinstance(row, str):
                continue

            if row in ['true', 'false']:
                dfr_[column] = dfr_.apply(
                    lambda x: 1 if x[column] == 'true' else 0,
                    axis=1)
                dtype.update({column: Boolean})
                break

            elif GUID_PATTERN.fullmatch(row) is not None:
                dfr_[column] = dfr_.apply(
                    lambda x: utils.unhexlify(x[column]),
                    axis=1)

                dtype.update({column: LargeBinary})
                break

            elif DATE_1C_PATTERN.fullmatch(row) is not None:
                dfr_[column] = dfr_.apply(
                    lambda x: utils.parse_date(x[column]),
                    axis=1)
                dtype.update({column: DateTime})
                break

    return dfr_, dtype


def process_element(elem):
    """
    Обработка элемента контекста. Элементы Справочников и Документов без
    дополнительной вложенности, поэтому один цикл собирает атрибуты элемента и
    возвращает dict. Элементы Регистров Накопления и Сведений содержат
    дополнительную вложенность, поэтому требуется вложенный цикл, и
    возвращается уже не dict, а list, который содержит элементы dict.
    """

    if any(tag in elem.tag for tag in ('CatalogObject', 'DocumentObject')):
        data = {}
        for child in elem:
            if len(child) > 0:
                data[child.tag] = '[табличный документ]'
            else:
                data[child.tag] = child.text

        return data

    # elif any(tag in elem.tag for tag in ('InformationRegisterRecordSet',
    #                                     'AccumulationRegisterRecordSet')):

    filter_ = {}
    for child in elem[0]:
        if len(child) > 0:
            filter_['{}_f'.format(child.tag)] = '[табличный документ]'
        else:
            filter_['{}_f'.format(child.tag)] = child.text

    data_list = []
    for subelem in elem[1]:
        data = {}
        for child in subelem:
            if len(child) > 0:
                data[child.tag] = '[табличный документ]'
            else:
                data[child.tag] = child.text
        data.update(filter_)
        data_list.append(data)
    return data_list


def get_message_no(context) -> int:
    """
    Парсит до первого вхождения элементы контекста и возвращает номер файла,
    который является уникальным и инкрементным для обработки в базе
    """
    message_no_: int = -1
    for event, elem in context:
        if event == "end" and \
                elem.tag == "{http://v8.1c.ru/messages}MessageNo":
            message_no_ = int(elem.text)
            break
    return message_no_


def inspect_xml(folder_):
    """
    Функция для режима отладки, содержит ограничения на кол-во инспектируемых
    элементов, служит для дебага и исследования структуры элементов контекста.
    """
    myconn = MysqlConn('conf.json', 'sec.json')
    myconn.connect()

    xmls = [f for f in listdir(folder_) if isfile(join(folder_, f))]
    for xml_file in xmls:
        xml = XmlFile('{}{}'.format(folder_, xml_file))
        msg_no = xml.describe().msg_no

        filter_name = ('InformationRegisterRecordSet.'
                       'ОграничениеАссортиментаПодразделения')

        for rows, _ in xml.iter_xml(
                filter_elem=filter_name):
            dfr = pd.DataFrame(rows)
            dfr['msg_no'] = msg_no

            try:
                dfr.to_sql(
                    'test_ОграничениеАссортиментаПодразделения',
                    con=myconn.engine,
                    if_exists='append',
                    chunksize=1000
                    )

            except SQLAlchemyError as err:
                error = str(err.__dict__['orig'])
                print(error)
                return

    myconn.dispose()


def describe_xml(context):
    """
    Возвращает словарь вида Объект - Количество элементов. Элементы контекста
    Регистр Сведений и Накопления содержат вложенные строки, поэтому сделан
    дополнительный цикл.
    """

    groups = {}

    for count, context_tuple in enumerate(context):
        event, elem = context_tuple

        if count % 1000000 == 0:
            sys.stdout.write('\r')
            sys.stdout.write("{}m elements done ".format(count/1000000))
            sys.stdout.flush()

        # if count == 20000000:
        #   break

        if event == "end" and any(
                table_name in elem.tag for table_name in CML_TABLES
        ):
            if any(tag in elem.tag for tag in (
                    'CatalogObject', 'DocumentObject')):
                try:
                    groups[elem.tag] += 1
                except KeyError:
                    groups[elem.tag] = 1

            elif any(tag in elem.tag for tag in (
                    'InformationRegisterRecordSet',
                    'AccumulationRegisterRecordSet')):
                for _ in elem[1]:
                    try:
                        groups[elem.tag] += 1
                    except KeyError:
                        groups[elem.tag] = 1

            elem.clear()
            # Also eliminate now-empty references from the root node to elem
            for ancestor in elem.xpath('ancestor-or-self::*'):
                while ancestor.getprevious() is not None:
                    del ancestor.getparent()[0]

    del context
    return groups


def scan_folder(folder_):
    """
    Сканирование папки на номера сообщений в файлах
    Сканирует указанную папку и выводит список XML файл - msg_no
    """
    xml_props = namedtuple('xml_prop', 'msg_no file_name')
    xml_dict = {}

    xmls = [f for f in listdir(folder_) if isfile(join(folder_, f))]
    for xml in xmls:
        LOG.debug('file: {}'.format(xml))

        if xml.split('.')[1] != 'xml':
            continue

        context = etree.iterparse("{}\\{}".format(folder_, xml),
                                  events=('end',))
        xml_dict[xml] = get_message_no(context)

    for value, key in sorted(xml_props(v, k) for (k, v) in xml_dict.items()):
        LOG.info('{} {}'.format(key, value))


def upload_rests(file_, engine_):
    """
    Загрузка файла дневных остатков
    """
    with open(file_, encoding="utf-8") as file:
        dfr = pd.read_csv(
            file,
            engine="c",
            sep='\t',
            encoding='utf-8',
            converters={'dat': utils.s2d,
                        'КоличествоОстаток': utils.s2f,
                        'РезервОстаток': utils.s2f,
                        'СуммаРознОстаток': utils.s2f,
                        'СуммаРознУпрОстаток': utils.s2f}
            )

    pref = utils.get_prefix_from_file(file_)
    try:
        dfr.to_sql(
            '{}_rests_checkpoint'.format(pref),
            con=engine_,
            if_exists='append',
            chunksize=1000
            )

        return True, None

    except SQLAlchemyError as err:
        error = str(err.__dict__['orig'])
        return False, error


def upload_excel(engine_, file_, table_name, sheet_name='Лист1'):
    """
    Загрузка файла-выгрузки Excel в указанную таблицу
    """
    dfr = pd.read_excel(file_, sheet_name=sheet_name)
    dfr, dtype = convert_df(dfr, None)

    try:
        dfr.to_sql(
            table_name,
            con=engine_,
            if_exists='replace',
            chunksize=1000,
            dtype=dtype,
            index=False
            )

        report = 'файл {} успешно залит'.format(file_)
        return True, report

    except SQLAlchemyError as err:
        error = str(err.__dict__['orig'])
        return False, error


def dir_scan_mode(engine_):
    LOG.debug('def dir_scan_mode')

    with open('conf.json') as file:
        conf = json.load(file)

    shared_folder_path = Path(conf['shared_folder'])
    LOG.debug('shared_folder_path: {}'.format(shared_folder_path))

    xml_files = shared_folder_path.glob('*.xml')
    xml_list = []

    for xml in xml_files:
        xml_file = XmlFile(str(xml.absolute()))
        _, file_name, msg_no = xml_file.describe(ext=False)
        xml_list.append((file_name, msg_no))

    for key, value in OrderedDict(xml_list).items():
        LOG.debug('{} {}'.format(key, value))

        myconn = MysqlConn('conf.json', 'sec.json')
        myconn.connect()

        context = etree.iterparse('{}{}'.format(conf['shared_folder'], key),
                                  events=('end',))

        status, report, message_no = iter_xml(
            context,
            process_element,
            myconn.engine,
            '{}{}'.format(conf['shared_folder'], key))

        LOG.debug('status {}, report: {}, message_no: {}'.format(
            status, report, message_no))

        myconn.dispose()

    return True, None


def starter(func, *args, create_sql_conn=True):

    if create_sql_conn:
        myconn = MysqlConn('conf.json', 'sec.json')
        myconn.connect()
        status, report = func(myconn.engine, *args)
        myconn.dispose()

    return status, report


def main(args_):
    """
    Основная функция, маршрутизирует движение скрипта в зависимости от
    переданных аргументов
    """
    if args_.single_file_mode:

        if not args_.file:
            print('file is empty')
            return

        myconn = MysqlConn('conf.json', 'sec.json')
        myconn.connect()

        context = etree.iterparse(args_.file, events=('end',))

        status, report, message_no = iter_xml(
            context, process_element, myconn.engine, args_.file)
        print('status {}, report: {}, message_no: {}'.format(
            status, report, message_no))

        myconn.dispose()

    elif args_.inspect_mode:
        inspect_xml(args_.folder)

    elif args_.describe_xml:

        if not args_.file:
            print('file is empty')
            return

        context = etree.iterparse(args_.file, events=('end',))

        print('\n')
        for key, value in describe_xml(context).items():
            print('{}: {}'.format(key, value))

    elif args_.scan_folder:
        if not args_.folder:
            print('folder is empty')
            return

        scan_folder(args_.folder)

    elif args_.upload_rests:
        if not args_.file:
            print('file is empty')
            return

        myconn = MysqlConn('conf.json', 'sec.json')
        myconn.connect()
        status, report = upload_rests(args_.file, myconn.engine)
        print(status, report)
        myconn.dispose()

    elif args_.upload_excel:
        if not args_.file or not args_.table_name:
            print('file is empty')
            return

        status, report = starter(
            upload_excel, args_.file, args_.table_name, create_sql_conn=True)
        print(status, report)

    elif args_.dir_scan_mode:
        LOG.info('start dir_scan_mode')
        status, report = starter(dir_scan_mode)
        print(status, report)
