# -*- coding: utf-8 -*-
"""
Служебные функции для парсера xml выгрузок из 1С commercml_upld.py

@author: syshchenko
"""

import sys
import binascii
from datetime import datetime

PREFIX_NAME = {
        'РарусТКПТ': 'rar',
        'Розница': 'roz'
        }


def s2f(xml_float: str) -> float:
    """
    Конвертирует строку с запятой и какиим-то пробелом в тип float
    """
    if isinstance(xml_float, str):
        try:
            return float(xml_float.replace('\xa0', '').replace(',', '.'))
        except ValueError:
            return 0.0
    return None


def s2d(xml_datetime: str):
    """
    Конвертирует строку с датой в тип datetime
    """
    if isinstance(xml_datetime, str):
        return datetime.strptime(xml_datetime, '%d.%m.%Y %H:%M:%S')
    return None


def unhexlify(elem: str) -> bytes:
    """
    Конвертирует строку с текстовым guid в тип bytes
    """
    if isinstance(elem, str):
        return binascii.unhexlify(elem.replace('-', ''))
    return elem


def parse_date(elem: str):
    """
    Парсит дату из выгрузки в  формате 1С с букво Т между датой и временем в
    тип datetime
    """
    if isinstance(elem, str):
        return datetime.strptime(elem, '%Y-%m-%dT%H:%M:%S')
    return elem


def get_prefix_from_file(file_name) -> str:
    """
    Парсит путь файла и выбирает по ключу сокращенный префикс
    """
    try:
        return PREFIX_NAME[file_name.split('/')[-1].split('_')[0]]
    except KeyError:
        return PREFIX_NAME[file_name.split('\\')[-1].split('_')[0]]


def get_file_name_from_path(file_path: str) -> str:
    """
    Парсит имя файла из пути
    """
    if len(file_path.split('/')) > 1:
        return file_path.split('/')[-1]
    else:
        return file_path.split('\\')[-1]


def counter(count_):
    if count_ % 1000000 == 0:
        sys.stdout.write('\r')
        sys.stdout.write("{}m elements done ".format(count_/1000000))
        sys.stdout.flush()
